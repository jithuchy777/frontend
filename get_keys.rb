require 'aws-sdk-s3'

Aws.config[:credentials] = Aws::Credentials.new(
  ENV['ACCESSKEYID'],
  ENV['SECRETACCESSKEY']
)
s3 = Aws::S3::Client.new(region:'us-east-2')
puts ENV["HOME"]
Aws.config.update(
  region:'us-east-2')

s3.get_object({bucket:'sumedha', key:'keys/myaccesskey.json'}, target: ENV['HOME']+'/keys/android/myaccesskey.json')
s3.get_object({bucket:'sumedha', key:'keys/signkey.jks'}, target: ENV['HOME']+'/keys/android/signkey.jks')
