import 'package:Sumedha/models/progress.dart';
import 'package:Sumedha/services/api_service.dart';
import 'package:Sumedha/utilities/constants/string_constants.dart';
import 'package:Sumedha/utilities/storage_helper.dart';
import 'package:flutter/cupertino.dart';

class ProgressProvider extends ChangeNotifier {
  ProgressModel progress;
  ProgressProvider();
  StorageHelper _storageHelper = StorageHelper();
  DataSource _dataSource = DataSource();

  Future<ProgressModel> getProgress() async {
    await getNewAccessToken();
    String access = await _storageHelper.getString(AuthStringConstants.access);
    progress = await _dataSource.getProgress(access: access);
    notifyListeners();
    return progress;
  }

  Future<void> getNewAccessToken() async {
    String refresh =
        await _storageHelper.getString(AuthStringConstants.refresh);
    await _dataSource.refresh(refresh).then((value) async => {
          await _storageHelper.saveString(
              key: AuthStringConstants.access, value: value.access)
        });
  }
}
