import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Sumedha/utilities/screen_size.dart';

class TestCompletedWidget extends StatefulWidget {
  @override
  _TestCompletedWidgetState createState() => _TestCompletedWidgetState();
}

class _TestCompletedWidgetState extends State<TestCompletedWidget> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(10),
              child: Icon(
                Icons.check_box,
                size: 120,
                color: Colors.green[800],
              ),
            ),
            Text(
              "Test Completed",
              style: Theme.of(context).textTheme.headline5,
            ),
            SizedBox(
              height: 8,
            ),
            Text(
              "Results will be published Shortly",
              style: Theme.of(context).textTheme.subtitle2,
            ),
            ViewTestResult(),
          ],
        ),
      ),
    );
  }
}

class ViewTestResult extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ScreenSize screenSize = ScreenSize(context);
    return Container(
      margin: EdgeInsets.only(
          top: 8,
          bottom: 24,
          left: screenSize.width / 8,
          right: screenSize.width / 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          RaisedButton(
            padding: EdgeInsets.symmetric(vertical: 20),
            elevation: 3,
            onPressed: () {},
            child: Text(
              'View Result',
              style: Theme.of(context).textTheme.subtitle1,
            ),
            color: Theme.of(context).colorScheme.background,
            shape: RoundedRectangleBorder(
              side: BorderSide(
                  color: Theme.of(context).textTheme.headline1.color),
              borderRadius: BorderRadius.circular(30),
            ),
          ),
        ],
      ),
    );
  }
}
