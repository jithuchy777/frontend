import 'package:Sumedha/UI/screens/test/tests_page_widget.dart';
import 'package:Sumedha/models/exams.dart';
import 'package:Sumedha/models/student_exam.dart';
import 'package:Sumedha/providers/exam_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TestsPage extends StatefulWidget {
  @override
  _TestsPageState createState() => _TestsPageState();
}

class _TestsPageState extends State<TestsPage>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    super.initState();

    _tabController = TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    final examProvider = Provider.of<ExamProvider>(context);

    return Scaffold(
        body: RefreshIndicator(
      onRefresh: () async {
        examProvider.refreshExams();
      },
      child: Column(
        children: [
          SizedBox(
            height: 16,
          ),
          Container(
            height: 50,
            margin: EdgeInsets.only(top: 8),
            color: Theme.of(context).bottomAppBarTheme.color,
            child: TabBar(
                controller: _tabController,
                indicatorColor: Theme.of(context).iconTheme.color,
                indicatorSize: TabBarIndicatorSize.tab,
                tabs: [
                  Container(
                    child: Text(
                      'Pending',
                      style: Theme.of(context).textTheme.headline6,
                    ),
                  ),
                  Container(
                    child: Text(
                      'Completed',
                      style: Theme.of(context).textTheme.headline6,
                    ),
                  ),
                ]),
          ),
          SizedBox(
            height: 24,
          ),
          Flexible(
            child: TabBarView(
              controller: _tabController,
              children: [
                pendingExamTab(context),
                completedExamTab(context),
              ],
            ),
          ),
        ],
      ),
    ));
  }

  Widget pendingExamTab(BuildContext context) {
    final examProvider = Provider.of<ExamProvider>(context);
    return FutureBuilder<List<Exam>>(
        future: examProvider.getAllExams(),
        builder: (context, snapshot) {
          if (snapshot.hasData &&
              snapshot.connectionState == ConnectionState.done) {
            if (snapshot.data.length == 0) {
              return Text('No Pending Exams');
            }
            return ListView.builder(
              shrinkWrap: true,
              itemCount: snapshot.data.length,
              itemBuilder: (context, int index) {
                return TestCard(
                  exam: snapshot.data[index],
                );
              },
            );
          } else
            return Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
        });
  }

  Widget completedExamTab(BuildContext context) {
    final examProvider = Provider.of<ExamProvider>(context);
    return FutureBuilder<List<StudentExam>>(
        future: examProvider.getStudentExams(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.length == 0) {
              return Text('No Completed exams');
            }
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (context, int index) {
                return StudentExamCard(
                  exam: snapshot.data[index],
                );
              },
            );
          } else
            return Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
        });
  }
}

