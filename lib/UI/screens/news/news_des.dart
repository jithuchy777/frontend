import 'package:Sumedha/UI/components/appbar.dart';
import 'package:Sumedha/UI/screens/news/news_cards_widgets.dart';
import 'package:Sumedha/models/feed.dart';
import 'package:Sumedha/providers/feeds_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class NewsDescription extends StatelessWidget {
  final FeedsList feed;

  const NewsDescription({Key key, this.feed}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final feeds = Provider.of<FeedsProvider>(context);
    return Scaffold(
      appBar: SumedhaAppBar(),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 16.0, bottom: 8),
            child: Center(
              child: Text(
                'Headlines on ' + feed.name,
                style: Theme.of(context).textTheme.headline5,
              ),
            ),
          ),
          FutureBuilder<List<Feed>>(
              future: feeds.getFeedForID(feed.id),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  if (snapshot.data.length == 0) {
                    return Text('No news Found');
                  }

                  return Flexible(
                    child: ListView.builder(
                        itemCount: snapshot.data.length,
                        itemBuilder: (context, index) => NewsCard(
                              feed: snapshot.data[index],
                            )),
                  );
                } else
                  return CircularProgressIndicator();
              }),
        ],
      ),
    );
  }
}
