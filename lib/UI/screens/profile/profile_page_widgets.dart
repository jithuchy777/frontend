import 'package:Sumedha/UI/screens/profile/previous_scores.dart';
import 'package:Sumedha/UI/screens/profile/user_details.dart';
import 'package:Sumedha/models/profile_data.dart';
import 'package:Sumedha/models/student_exam.dart';
import 'package:Sumedha/providers/profile_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProfilePageWidgets extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final profileProvider = Provider.of<ProfileProvider>(
      context,
    );
    return RefreshIndicator(
      onRefresh: () async {
        profileProvider.refresh();
      },
      child: Column(
        children: [
          FutureBuilder<List<ProfileData>>(
            future: profileProvider.getUserName(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return UserDetails(
                  name: snapshot.data[0].username,
                  userName: snapshot.data[0].name,
                );
              } else
                return Center(
                  child: CircularProgressIndicator(),
                );
            },
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: FutureBuilder<List<StudentExam>>(
                future: profileProvider.getProfileData(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return ListView(
                      children: [
                        ListView.builder(
                          shrinkWrap: true,
                          itemCount:
                              snapshot.data == null ? 0 : snapshot.data.length,
                          physics: BouncingScrollPhysics(),
                          itemBuilder: (context, index) {
                            return PreviousScoreCard(
                              score: snapshot.data[index].score.toString() ==
                                      'null'
                                  ? 'NA'
                                  : snapshot.data[index].score.toString(),
                              testName: snapshot.data[index].name,
                            );
                          },
                        ),
                      ],
                    );
                  } else
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
