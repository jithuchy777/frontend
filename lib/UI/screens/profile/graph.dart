// import 'package:Sumedha/models/student_exam.dart';
// import 'package:Sumedha/utilities/screen_size.dart';
// import 'package:charts_flutter/flutter.dart' as charts;
// import 'package:flutter/material.dart';

// class BarGraphDemo extends StatefulWidget {
//   final List<StudentExam> data;
//   BarGraphDemo({this.data});
//   @override
//   _BarGraphDemoState createState() => _BarGraphDemoState();
// }

// class _BarGraphDemoState extends State<BarGraphDemo> {
//   List<AppDownloads> data;
//   ScreenSize screenSize;

//   @override
//   void initState() {
//     super.initState();

//     // data = [
//     //   AppDownloads(
//     //     year: '2017',
//     //     count: 178.1,
//     //     barColor: charts.ColorUtil.fromDartColor(Colors.blue),
//     //   ),
//     //   AppDownloads(
//     //     year: '2018',
//     //     count: 205.4,
//     //     barColor: charts.ColorUtil.fromDartColor(Colors.blue),
//     //   ),
//     //   AppDownloads(
//     //     year: '2019',
//     //     count: 258.2,
//     //     barColor: charts.ColorUtil.fromDartColor(Colors.blue),
//     //   ),
//     // ];
//   }

//   @override
//   Widget build(BuildContext context) {
//     screenSize = ScreenSize(context);

//     return Column(
//       mainAxisAlignment: MainAxisAlignment.center,
//       crossAxisAlignment: CrossAxisAlignment.center,
//       children: <Widget>[
//         Container(
//           height: screenSize.height / 3,
//           width: double.infinity,
//           padding: const EdgeInsets.all(12),
//           child: SizedBox(
//             width: 20,
//             child: Card(
//               child: MyBarChart(data),
//             ),
//           ),
//         ),
//       ],
//     );
//   }
// }

// class AppDownloads {
//   final String year;

//   final double count;

//   final charts.Color barColor;

//   AppDownloads({
//     @required this.year,
//     @required this.count,
//     @required this.barColor,
//   });
// }

// class MyBarChart extends StatelessWidget {
//   final List<AppDownloads> data;

//   MyBarChart(this.data);

//   @override
//   Widget build(BuildContext context) {
//     List<charts.Series<AppDownloads, String>> series = [
//       charts.Series(
//           id: 'perfomance',
//           data: data,
//           domainFn: (AppDownloads downloads, _) => downloads.year,
//           measureFn: (AppDownloads downloads, _) => downloads.count,
//           colorFn: (AppDownloads downloads, _) => downloads.barColor)
//     ];

//     return charts.BarChart(
//       series,
//       animate: true,
//       barGroupingType: charts.BarGroupingType.groupedStacked,
//     );
//   }
// }
