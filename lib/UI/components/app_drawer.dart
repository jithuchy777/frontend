import 'package:Sumedha/UI/common/alert_dialog.dart';
import 'package:Sumedha/UI/common/themes/base_theme.dart';
import 'package:Sumedha/providers/auth_provider.dart';
import 'package:Sumedha/providers/theme_changer.dart';
import 'package:Sumedha/utilities/constants/enums.dart';
import 'package:Sumedha/utilities/constants/string_constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _auth = Provider.of<AuthService>(context, listen: true);
    final theme = Provider.of<Themer>(context);
    return Drawer(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              DrawerHeader(
                child: Container(child: Image.asset(LogoStringConstants.logo)),
              ),
              ListTile(
                  trailing: Switch(
                      value: theme.appTheme != AppTheme.Light,
                      onChanged: (v) {
                        if (v)
                          theme.setTheme(BaseTheme.darkTheme);
                        else
                          theme.setTheme(BaseTheme.lightTheme);
                      }),
                  leading: Icon(Icons.settings),
                  title: Text("Dark Mode"),
                  onTap: null),
              ListTile(
                leading: Icon(Icons.exit_to_app),
                onTap: () async {
                  Widget cancelButton = FlatButton(
                    child: Text("Cancel"),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  );
                  Widget continueButton = FlatButton(
                    child: Text("Confirm"),
                    onPressed: () async {
                      Navigator.of(context).pop();
                      Navigator.pop(context);
                      Navigator.of(context).pushNamedAndRemoveUntil(
                          '/login', (Route route) => false);
                      await _auth.signOut();
                      SystemNavigator.pop();
                    },
                  );
                  AppAlert.showAlertDialog(
                      context: context,
                      title: 'Are you Sure ?',
                      subtitle: "You will be logged out from your account",
                      cancelButton: cancelButton,
                      continueButton: continueButton);
                },
                title: Text("Sign Out"),
              ),
              ListTile(
                leading: Icon(Icons.transit_enterexit),
                title: Text('Exit app'),
                onTap: () => SystemNavigator.pop(),
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: Column(
              children: [
                Divider(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    IconButton(
                      icon: FaIcon(
                        FontAwesomeIcons.facebook,
//                        color: Color(0xff4267B2),
                        color: Color(0xff777777),
                      ),
                      onPressed: () =>
                          _launchURL('http://facebook.com/kitesfoundationfb'),
                    ),
                    IconButton(
                      icon: FaIcon(
                        FontAwesomeIcons.twitter,
//                        color: Color(0xff1DA1F2),
                        color: Color(0xff777777),
                      ),
                      onPressed: () =>
                          _launchURL('https://twitter.com/FoundationKites'),
                    ),
                    IconButton(
                      icon: FaIcon(
                        FontAwesomeIcons.instagram,
//                        color: Color(0xffE1306C),
                        color: Color(0xff777777),
                      ),
                      onPressed: () =>
                          _launchURL('https://instagram.com/kitesfoundation'),
                    ),
                    IconButton(
                      icon: FaIcon(
                        FontAwesomeIcons.youtube,
//                        color: Color(0xffFF0000),
                        color: Color(0xff777777),
                      ),
                      onPressed: () => _launchURL(
                          'https://www.youtube.com/channel/UCoyxzgTZee1t068TJ0o44fg'),
                    ),
                    IconButton(
                      icon: FaIcon(
                        FontAwesomeIcons.linkedin,
//                        color: Color(0xff2867B2),
                        color: Color(0xff777777),
                      ),
                      onPressed: () => _launchURL(
                          'https://www.linkedin.com/company/kites-foundation'),
                    ),
                  ],
                ),
                SizedBox(
                  height: 8,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Made with '),
                    Icon(
                      Icons.favorite_border,
                      color: Colors.red,
                    ),
                    Text(' by '),
                    SizedBox(
                      width: 5,
                    ),
                    GestureDetector(
                      child: Container(
                          child: Image.asset(
                        theme.logo,
                        height: 30,
                      )),
                      onTap: () => _launchURL('https://studevsoc.com/'),
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}

class ExamAppDrawer extends StatelessWidget {
  const ExamAppDrawer({
    Key key,@required this.testName, @required this.testDescription, @required this.totalTime, @required this.totalQns,
  }) : super(key: key);

  final String testName, testDescription;
  final int totalTime, totalQns;

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          DrawerHeader(
            child: Container(child: Image.asset(LogoStringConstants.logo)),
          ),
          SizedBox(
            height: 16,
          ),
          ListTile(
            title: Text(testName),
            subtitle: Text("Test Name"),
          ),

          ListTile(
            title: Text(testDescription),
            subtitle: Text("Test Description"),
          ),

          ListTile(
            title: Text(totalTime.toString()),
            subtitle: Text("Total Time"),
          ),

          ListTile(
            title: Text(totalQns.toString()),
            subtitle: Text("Total Questions"),
          ),
          SizedBox(
            height: 16,
          ),
          ListTile(
            leading: Material(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)),
              color: Colors.white,
              elevation: 2,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  backgroundColor: Colors.white,
                  child: Text(
                    '1',
                    style: TextStyle(color: Theme.of(context).iconTheme.color),
                  ),
                ),
              ),
            ),
            title: Text(
              'Un-Attended',
            ),
          ),
          SizedBox(
            height: 16,
          ),
          ListTile(
            leading: Material(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)),
              color: Colors.white,
              elevation: 2,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  child: Text('2',
                      style:
                          TextStyle(color: Theme.of(context).iconTheme.color)),
                  backgroundColor: Colors.green,
                ),
              ),
            ),
            title: Text('Answered'),
          ),
          SizedBox(
            height: 16,
          ),
          ListTile(
            leading: Material(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)),
              color: Colors.white,
              elevation: 2,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  child: Text('3',
                      style:
                          TextStyle(color: Theme.of(context).iconTheme.color)),
                  backgroundColor: Colors.orange,
                ),
              ),
            ),
            title: Text('Skipped'),
          ),
          SizedBox(
            height: 16,
          ),
          ListTile(
            leading: Material(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)),
              color: Colors.white,
              elevation: 2,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  child: Text('4',
                      style:
                          TextStyle(color: Theme.of(context).iconTheme.color)),
                  backgroundColor: Colors.purple,
                ),
              ),
            ),
            title: Text('Marked and Submitted'),
          ),
        ],
      ),
    );
  }
}
