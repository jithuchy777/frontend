// To parse this JSON data, do
//
//     final feed = feedFromMap(jsonString);

import 'dart:convert';

List<Feed> feedFromMap(String str) =>
    List<Feed>.from(json.decode(str).map((x) => Feed.fromMap(x)));

String feedToMap(List<Feed> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

class Feed {
  Feed({
    this.title,
    this.summary,
    this.published,
    this.link,
  });

  String title;
  String summary;
  DateTime published;
  String link;

  Feed copyWith({
    String title,
    String summary,
    DateTime published,
    String link,
  }) =>
      Feed(
        title: title ?? this.title,
        summary: summary ?? this.summary,
        published: published ?? this.published,
        link: link ?? this.link,
      );

  factory Feed.fromMap(Map<String, dynamic> json) => Feed(
        title: json["title"],
        summary: json["summary"],
        published: DateTime.parse(json["published"]),
        link: json["link"],
      );

  Map<String, dynamic> toMap() => {
        "title": title,
        "summary": summary,
        "published": published.toIso8601String(),
        "link": link,
      };
}

List<FeedsList> feedsListFromMap(String str) =>
    List<FeedsList>.from(json.decode(str).map((x) => FeedsList.fromMap(x)));

String feedsListToMap(List<FeedsList> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

class FeedsList {
  FeedsList({
    this.id,
    this.name,
  });

  int id;
  String name;

  FeedsList copyWith({
    int id,
    String name,
  }) =>
      FeedsList(
        id: id ?? this.id,
        name: name ?? this.name,
      );

  factory FeedsList.fromMap(Map<String, dynamic> json) => FeedsList(
        id: json["id"],
        name: json["name"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
      };
}
